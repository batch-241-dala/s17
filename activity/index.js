/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	function getUserInput() {
		let name = prompt("What is your name?");
		let age = prompt("Please tell me your age?");
		let address =prompt("Where do you live?");
		alert("Thank you for your answers");

		console.log("Hello, " + name);
		console.log("You are " + age + " years old");
		console.log("You live in " + address);
	}

	getUserInput();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	function favoriteBands() {
		console.log("1. December Avenue");
		console.log("2. Agsunta");
		console.log("3. Ben&Ben");
		console.log("4. South Border");
		console.log("5. Eraserheads");
	
	}

	favoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	function favoriteMovies() {
		console.log("1. Stranger Things");
		console.log("Rotten Tomatoes Rating: 92%");
		console.log("2. Jumanji");
		console.log("Rotten Tomatoes Rating: 71%");
		console.log("3. Shutter Island");
		console.log("Rotten Tomatoes Rating: 68%");
		console.log("4. Enola Holmes");
		console.log("Rotten Tomatoes Rating: 94%");
		console.log("5. Dead Pool");
		console.log("Rotten Tomatoes Rating: 84%");
	}

	favoriteMovies();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with: ")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();