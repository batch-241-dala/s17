/*
Functions
	Functions in javascript are lines/block of codes that tell our device/application to perform a certain task when called/invoked

	Functions are mostly created to create complicated tasks to run several lines of code in a succession.

	Functions are also used to prevent repeating lines/block of codes that perform the same task/function

*/

// Function Declaration
	// (function statement) defines a function with the specified parameters
/*
	Syntax:
		function functionName() {
			codeblock(statement);
		}

	function keyword - used to defined a javascript functions
	functionName - the function name.Function are named to be able to use later in the code
	function block ({}) - the statements which comprise the body of the function. This is where the code to be executed.

*/

function printName() {
	console.log("My name is Arjay!");
}

// Function Invocation
/*
	The code block and statements inside a function is not immediately executed when the function is defined. The code block and statements insdie a function is executed when the function is invoke or called.
*/

// Invoke/call the function that we declared
printName();




// Function Declaration vs Function Expressions
	// Function Declaration
		// A function can be created through function declaration by using the function keyword and adding the function name
		// Declared function are NOT executed immediately. They are "saved for later used" and will be executed later when they are invoked(called).

	declaredFunction(); //Invoke function before declaration
	
	function declaredFunction(){
		console.log("Hello World from the declaredFunction");
	}

	declaredFunction();

	// Function Expressions
		// A function can also be stored in a variable. This is called function expression

		// A function expression is an anonymous function assigned to the variableFunction

		// Anonymous function - a function without a name

		// Cannot be invoked before declaration

	let variableFunction = function() {

		console.log("Hello from the variableFunction");

	}

	variableFunction();

		// Function expressions are ALWAYS invoked(called) using the variable name.

	let funcExpression = function funcName() {
		console.log("Hello from the other side!");
	}

	funcExpression();

	// Can we reassign declared functions and function expressions to a NEW anonymous functions?

		// Reassigning declared function from previous example line 44
	declaredFunction = function() {
		console.log("Updated declaredFunction");
	}

	declaredFunction();

		// Reassigning function expression from previous example line 59
	funcExpression = function() {
		console.log("Updated funcExpression");
	}

	funcExpression();

		// Constant Function
	const constantFunc = function() {
		console.log("Initialized with CONST");
	}

	constantFunc();
		// Note: constant function cannot be reassigned




// Function Scoping
/*
	Scope is the accessibility (visibility) of variables

	JS Variables has 3 types of scope
		1. local/block scope
		2. global scope
		3. function scope

*/

	{
		let localVar = "I am a local variable,";
	}

	let globalVar = "I am a global variable."

	console.log(globalVar);
// console.log(localVar);
// // Uncaught ReferenceError: localVar is not defined

	function showNames() {
	// Function scoped variables

		var functionVar = "Joe";
		const functionConst = "John";
		let functionLet = "Jane";

		console.log(functionVar);
		console.log(functionConst);
		console.log(functionLet);

	}

	showNames();

// The variables, functionvar, functionConst and functionLet are function scoped variables. They can only be access inside of the function they were declared in.

	// console.log(functionVar);
	// console.log(functionConst);
	// console.log(functionLet);
	// index.js:136 Uncaught ReferenceError: functionVar is not defined
    // at index.js:136:14




// NESTED FUNCTION
	//  You can create another function inside a function. This is called a nested function. This nested function, being inside the myNewFunction will have access to the variable, "name" as they are within the same code block/scope.

	function myNewFunction(){
		let name = "Maria";

		function nestedFunction(){
			let nestedName = "Jose";
			console.log(name);
		}

		// console.log(nestedName); variable cannot be revoked outside of its function.
		nestedFunction();
	}
	myNewFunction();
	// nestedFunction(); this will cause an error

// Function and Global Scoped Variables

	// Global Variable
	let globalName = "Arjay";

	function myNewFunction2() {
		let nameInside = "Jenno";

		console.log(globalName);
	}

	myNewFunction2();
	// console.log(nameInside); This will cause an error because we cannot revoke a variable outside of its function.




// USING ALERT()
/*
	Syntax:
		alert(String_message);

*/
	// alert("Hello World");

	function showSampleAlert(){
		alert("Hello User!");
	}

	// showSampleAlert();

	console.log("I will only log in the console when the alert is dismissed");


// USING PROMPT()
/*
	Syntax:
		prompt("<DialogInString>");

*/

	// let samplePrompt = prompt("Enter your name:");

	// console.log("Hello, " + samplePrompt);
	// console.log(typeof samplePrompt);

// function printWelcomeMessage() {
// 	let firstName = prompt("Enter your First Name: ");
// 	let lastName = prompt("Enter your Last Name: ");

// 	// console.log("Hello " + firstName + ' '+ lastName + "!");
// 	// console.log("Welcome to my page!");

// 	alert("Hello " + firstName + ' '+ lastName + "!");
// 	alert("Welcome to my page!");
// }

// printWelcomeMessage();

// Function Naming Conventions
	// Name your functions in small caps. Follows camelCase when naming variables and functions

	function displayCarInfo() {
		console.log("Brand: Toyota");
		console.log("Type: Sedan");
		console.log("Price: 1, 500, 000");
	}

	displayCarInfo();

	// Function names should be definitive of the task it will perform. It usually contains a verb

	function getCourse() {
		let courses = ["Science 101", "Math 101", "English 101"];
		console.log(courses);
	}

	getCourse();

	// Avoid generic names to avoid confusion within your code.
	// Avoid pointless and inappropriate function names.